terraform {
  backend "s3" {
    bucket = "s3-website-tfstate"
    key    = "dev/s3-static-website/terraform.tfstate"
    region = "ap-south-1"
    dynamodb_table = "dev-s3-static-website"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.74"
    }
  }
}