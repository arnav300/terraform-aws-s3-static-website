# Configure the AWS Provider
provider "aws" {
  region = "ap-south-1"
#  profile = "test" # change to the correct one
#  access_key = "$TF_VAR_aws_access_key"
#  secret_key = "$TF_VAR_aws_secret_key"
}

module "s3-website" {
  source = "../../modules/s3-website"

  bucket_name = "bhati-s3-website"
}

output "website_endpoint" {
  value = module.s3-website.website_endpoint
}