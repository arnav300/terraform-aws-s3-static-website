# terraform-static-website-aws

- A Terraform module for publishing static websites on AWS' S3. It requires two files to be present in the same directory as this module is used.

- index.html : The homepage of the website.
- error.html : The page shown when errors occur.

# Usage

1) Running terraform from local machine :

- Clone this repo.
- Configure aws using : aws configure
- If you want to create a new bucket with your name, you can change the name of the bucket in "main.tf" file present in live/s3-website directory.
- Navigate to live/s3-website directory.
- terraform init
- terraform validate
- terraform plan
- terraform apply

2) Running from Gitlab

- Make sure you have defined proper AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY defined in your Gitlab CI settings.


# Outputs

 - website_endpoint: The public url of this website (http://bhati-s3-website-final.s3-website.ap-south-1.amazonaws.com/)

# Backend

- This website stores the state of Terraform in S3 bucket backed by Dynamo DB.
